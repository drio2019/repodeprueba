def factorial(n):
    """Calcula el factorial de un número n."""
    if n == 0 or n == 1:
        return 1
    else:
        return n * factorial(n - 1)

def main():
    """Función principal que calcula y muestra los factoriales del 1 al 10."""
    for i in range(1, 11):
        print(f"Factorial de {i} es {factorial(i)}")

if __name__ == "__main__":
    main()
